use wslogger;

CREATE TABLE `endpoint` (
  `key` varchar(20) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `endpointurl` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `environment` (
  `key` varchar(20) NOT NULL,
  `ipv4` varchar(20) DEFAULT NULL,
  `ipv6` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `pw` varchar(64) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `port` varchar(20) DEFAULT NULL,
  `label` varchar(64) DEFAULT NULL,
  `protocol` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `messagefilter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `regex` varchar(4000) DEFAULT NULL,
  `routingid` int(11) DEFAULT NULL,
  `direction` varchar(20) DEFAULT NULL,
  `indexnumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request` text DEFAULT NULL,
  `response` text DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `operation` varchar(1024) DEFAULT NULL,
  `requesttype` varchar(20) DEFAULT NULL,
  `groupid` varchar(1024) DEFAULT NULL,
  `requestheader` varchar(2048) DEFAULT NULL,
  `requestquery` varchar(2048) DEFAULT NULL,
  `responseheader` varchar(2048) DEFAULT NULL,
  `endpoint` varchar(1024) DEFAULT NULL,
  `client` varchar(1024) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `routingid` int(11) DEFAULT NULL,
  `sourceenv` varchar(20) DEFAULT NULL,
  `targetenv` varchar(20) DEFAULT NULL,
  `value01` varchar(1024) DEFAULT NULL,
  `value02` varchar(1024) DEFAULT NULL,
  `value03` varchar(1024) DEFAULT NULL,
  `value04` varchar(1024) DEFAULT NULL,
  `value05` varchar(1024) DEFAULT NULL,
  `value06` varchar(1024) DEFAULT NULL,
  `value07` varchar(1024) DEFAULT NULL,
  `value08` varchar(1024) DEFAULT NULL,
  `value09` varchar(1024) DEFAULT NULL,
  `value10` varchar(1024) DEFAULT NULL,
  `requestraw` blob DEFAULT NULL,
  `responseraw` blob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_routingid_index` (`routingid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

CREATE TABLE `routinginfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `targetenv` varchar(20) DEFAULT NULL,
  `sourceenv` varchar(20) DEFAULT NULL,
  `operation` varchar(1024) DEFAULT NULL,
  `targetendpoint` varchar(20) DEFAULT NULL,
  `sourceendpoint` varchar(20) DEFAULT NULL,
  `groupkey` varchar(20) DEFAULT NULL,
  `loopback` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `routinginfo_groupkey_index` (`groupkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` text DEFAULT NULL,
  `templateraw` blob DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `category` varchar(64) DEFAULT NULL,
  `header` varchar(2048) DEFAULT NULL,
  `label01` varchar(64) DEFAULT NULL,
  `label02` varchar(64) DEFAULT NULL,
  `label03` varchar(64) DEFAULT NULL,
  `label04` varchar(64) DEFAULT NULL,
  `label05` varchar(64) DEFAULT NULL,
  `label06` varchar(64) DEFAULT NULL,
  `label07` varchar(64) DEFAULT NULL,
  `label08` varchar(64) DEFAULT NULL,
  `label09` varchar(64) DEFAULT NULL,
  `label10` varchar(64) DEFAULT NULL,
  `value01` varchar(1024) DEFAULT NULL,
  `value02` varchar(1024) DEFAULT NULL,
  `value03` varchar(1024) DEFAULT NULL,
  `value04` varchar(1024) DEFAULT NULL,
  `value05` varchar(1024) DEFAULT NULL,
  `value06` varchar(1024) DEFAULT NULL,
  `value07` varchar(1024) DEFAULT NULL,
  `value08` varchar(1024) DEFAULT NULL,
  `value09` varchar(1024) DEFAULT NULL,
  `value10` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `automapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `routing_id` int(11) DEFAULT NULL,
  `templateid` int(11) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `target_routing_id` int(11) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `regex` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `automapping_template_id_fk` (`templateid`),
  KEY `automapping_routinginfo_id_fk` (`routing_id`),
  CONSTRAINT `automapping_routinginfo_id_fk` FOREIGN KEY (`routing_id`) REFERENCES `routinginfo` (`id`),
  CONSTRAINT `automapping_template_id_fk` FOREIGN KEY (`templateid`) REFERENCES `template` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `autonum` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`autoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;