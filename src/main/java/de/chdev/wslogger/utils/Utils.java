package de.chdev.wslogger.utils;

import de.chdev.wslogger.aswbxml.ActiveSyncWbxmlConverter;
import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.dao.Session;
import de.chdev.wslogger.exception.PersistenceConnectException;
import de.chdev.wslogger.exception.PersistenceReadException;
import de.chdev.wslogger.exception.WSLoggerBasicException;
import de.chdev.wslogger.model.Request;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class Utils {
	
	Log log = LogFactory.getLog(Utils.class);
	
	private static Utils instance = null;
	
	private Utils(){
		
	}
	
	public static Utils getInstance(){
		if (instance==null){
			instance = new Utils();
		}
		return instance;
	}

	public String getDecodedString(byte[] encodedByteArray, String contentType) {
		if (contentType.equalsIgnoreCase("application/vnd.ms-sync.wbxml")){
			log.debug("decoding byte array from wbxml");
			ActiveSyncWbxmlConverter converter = new ActiveSyncWbxmlConverter(encodedByteArray);
			return converter.getXmlString();
		} else {
			return new String(encodedByteArray, Charset.forName("UTF-8"));
		}
	}

	public byte[] getEncodedBytes(String decodedString, String contentType) {
		if (contentType.equalsIgnoreCase("application/vnd.ms-sync.wbxml")){
			log.debug("encoding string to wbxml");
			ActiveSyncWbxmlConverter converter = new ActiveSyncWbxmlConverter(decodedString);
			return converter.getWbxml();
		} else {
			return decodedString.getBytes();
		}
	}

	public String prepareContent(String content, Request request, PersistenceDao connection) throws PersistenceReadException, PersistenceConnectException {
		String result = content;
		
		Session session = null;
		
		// Replace value definitions
		String[] values = new String[10];
		values[0] = request.getValue01();
		values[1] = request.getValue02();
		values[2] = request.getValue03();
		values[3] = request.getValue04();
		values[4] = request.getValue05();
		values[5] = request.getValue06();
		values[6] = request.getValue07();
		values[7] = request.getValue08();
		values[8] = request.getValue09();
		values[9] = request.getValue10();
		
		DecimalFormat df = new DecimalFormat("00");
		for (int i = 0; i < values.length; i++) {
			result = result.replaceAll("##VALUE"+df.format(i+1)+"##", values[i]==null?"":values[i]);
		}
		
		try {
			
			session = connection.getSession().open();
			
			// Set special placeholders
			if (result.contains("##TICKETNUM##")){
				Long generatedTicketId = connection.getGeneratedTicketId(session);
				result = result.replaceAll("##TICKETNUM##", generatedTicketId.toString());
			}
			
			session.commit();
			
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(new Date());
			XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			result = result.replaceAll("##TIMESTAMP##", xmlDate.toXMLFormat());
		} catch (DatatypeConfigurationException e) {
			log.error("Error during date conversion.", e);
		} catch (WSLoggerBasicException e) {
			log.error("Error during ticketnumber generation", e);
		} finally {
			if (session!=null){
				session.close();
			}
		}

		return result;
	}

	/**
	 * This method extracts the implementation version information and returns it
	 *
	 * @return version information string
	 */
	public String getVersionInfo() {
		Enumeration resEnum;
		String version = "not set";
		try {
//	        resEnum = Thread.currentThread().getContextClassLoader().getResources(JarFile.MANIFEST_NAME);
			resEnum = this.getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
			while (resEnum.hasMoreElements()) {
				try {
					URL url = (URL)resEnum.nextElement();
					InputStream is = url.openStream();
					if (is != null) {
						Manifest manifest = new Manifest(is);
						Attributes mainAttribs = manifest.getMainAttributes();
						if(mainAttribs.getValue("Implementation-Version") != null && mainAttribs.getValue("Implementation-Title")!=null && mainAttribs.getValue("Implementation-Title").equals("de.chdev.wslogger")) {
							version = mainAttribs.getValue("Implementation-Version");
						}
					}
				}
				catch (Exception e) {
					// Silently ignore wrong manifests on classpath?
				}
			}
		} catch (IOException e1) {
			// Silently ignore wrong manifests on classpath?
		}
		return version;
	}
}
