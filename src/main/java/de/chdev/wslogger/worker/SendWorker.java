package de.chdev.wslogger.worker;

import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.dao.Session;
import de.chdev.wslogger.exception.PersistenceConnectException;
import de.chdev.wslogger.exception.PersistenceReadException;
import de.chdev.wslogger.exception.PersistenceWriteException;
import de.chdev.wslogger.exception.WSLoggerBasicException;
import de.chdev.wslogger.model.Request;
import de.chdev.wslogger.model.RoutingEntry;
import de.chdev.wslogger.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

public class SendWorker {

	Log log = LogFactory.getLog(SendWorker.class);

	PersistenceDao dbConnection;

	public SendWorker(PersistenceDao dbConnection){
		this.dbConnection = dbConnection;
	}

	public Request sendRequest(Request request) {
		
		Request resultRequest = null;
		String responseContentType = "";
		String requestContentType = "text/xml";

		try {
			RoutingEntry routing = request.getRouting();

			// Build endpoint url
			String url = request.getTargetEnv().getProtocol() + "://"
					+ request.getTargetEnv().getHostname() + ":"
					+ request.getTargetEnv().getPort()
					+ routing.getTargetEndpoint().getEndpointUrl()
                    + (request.getRequestquery()!=null&&!request.getRequestquery().isEmpty()?"?"+request.getRequestquery():"");
			log.debug("Send request "+request.getId()+" to url " + url);

			// Build request
			HttpRequestBase httpRequest;
			if (request.getRequesttype() == null || request.getRequesttype().isEmpty() || request.getRequesttype().equalsIgnoreCase("post")) {
				httpRequest = new HttpPost(url);

			} else if (request.getRequesttype().equalsIgnoreCase("options")){
				httpRequest = new HttpOptions(url);
			} else {
				httpRequest = new HttpGet(url);
			}

			// Set Header data
			if (request.getRequestheader()!=null){
				for (String headerLine : request.getRequestheader().split("\n")) {
					// add header to request
					String key = headerLine.substring(0, headerLine.indexOf(":"));
					if (key.equalsIgnoreCase("soapaction"))
						httpRequest.addHeader(key,
								headerLine.indexOf(":") >= 0 ? headerLine
										.substring(headerLine.indexOf(":") + 1)
										.trim() : "");
					if (key.equalsIgnoreCase("content-type"))
						requestContentType = headerLine.indexOf(":") >= 0 ? headerLine
								.substring(headerLine.indexOf(":") + 1)
								.trim() : "";
				}
			}

			// Prepare request content / replace placeholders
			String preparedContent = Utils.getInstance().prepareContent(request.getRequest(), request, dbConnection);
			if (request.getRequest()!=null && !request.getRequest().equals(preparedContent)){
				request.setRequestraw(Utils.getInstance().getEncodedBytes(preparedContent, requestContentType));
			}
			request.setRequest(preparedContent);

			// Set body
			if (httpRequest instanceof HttpEntityEnclosingRequestBase){
				ByteArrayEntity myEntity = new ByteArrayEntity(request.getRequestraw(), ContentType.create(requestContentType));
				((HttpEntityEnclosingRequestBase)httpRequest).setEntity(myEntity);
			}

			// Add routing to loopback cache
			if (routing.getLoopback()!=null && routing.getLoopback().equals("OUT")){
				ApplicationCache.getInstance().addRequestCacheEntry(routing.getGroupkey()+""+request.getGroupid(), request);
			}

			// Build and Call Request
			CloseableHttpResponse response;

			CloseableHttpClient client = SendWorkerManager.getInstance().getHttpClient(routing, request.getTargetEnv());
			response = client.execute(httpRequest, SendWorkerManager.getInstance().getHttpContext(routing, request.getTargetEnv()));
			


			// Process response from request call
			try {

				// Process response header
				StringBuilder sb = new StringBuilder();
				for (Header httpHeader : response.getAllHeaders()) {
					if (httpHeader.getName().equalsIgnoreCase("content-type")) responseContentType = httpHeader.getValue();
					sb.append(httpHeader.getName() + ": "
							+ httpHeader.getValue() + "\n");
				}
				request.setResponseheader(sb.toString());

				// Process response body
				HttpEntity responseEntity = response.getEntity();
                request.setResponseraw(EntityUtils.toByteArray(responseEntity));
				request.setResponse(Utils.getInstance().getDecodedString(request.getResponseraw(), responseContentType));

			} finally {
				response.close();
			}

			// Remove routing from loopback cache
			ApplicationCache.getInstance().removeRequestCacheEntry(
					routing.getGroupkey()+""+request.getGroupid());
			
			resultRequest = request;
			
			log.debug("Send request for "+ request.getId()+" finished");
		} catch (ClientProtocolException e) {
			log.error("Error for send message", e);
		} catch (IOException e) {
			log.error("Error for send message", e);
		} catch (Exception e){
			log.error("unknown exception occurred", e);
		}
		
		return request;
	}

	public void sendAllOutRequests() {

		Session session = null;
		try {
			session = dbConnection.getSession().open();
			List<Request> outgoingRequestList = dbConnection.getRequestListByStatus(session, "OUT");
			
			session.commit();
			
			for (Request request : outgoingRequestList) {
				if (sendRequest(request)!=null) {
					request.setStatus("OUT_DONE");
					
					session.open();
					request = dbConnection.saveRequest(session, request);
					session.commit();
				}

			}

		} catch (PersistenceReadException | PersistenceConnectException
				| PersistenceWriteException e) {
			log.error("Error sending outgoing requests",e);
		} catch (WSLoggerBasicException e) {
			log.error("Error sending outgoing requests",e);
		} finally{
			if (session!=null){
				session.close();
			}
		}
	}
	
}
