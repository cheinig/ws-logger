package de.chdev.wslogger.worker;

import de.chdev.wslogger.model.Environment;
import de.chdev.wslogger.model.RoutingEntry;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.util.HashMap;
import java.util.Map;

public class SendWorkerManager {

	private static SendWorkerManager instance = null;
	
	private Map<String, HttpClientConfiguration> httpClientConfigPool = new HashMap<String, HttpClientConfiguration>();
	
	
	private SendWorkerManager(){
	}
	
	public static SendWorkerManager getInstance(){
		if (instance == null){
			instance = new SendWorkerManager();
		}
		return instance;
	}
	
	public CloseableHttpClient getHttpClient(RoutingEntry routing, Environment targetEnv){
		
		CloseableHttpClient resultClient = null;
		
		// Add new empty configuration
		if (!httpClientConfigPool.containsKey(routing.getId().toString())){
			httpClientConfigPool.put(routing.getId().toString(), new HttpClientConfiguration());
		}
		HttpClientConfiguration config = httpClientConfigPool.get(routing.getId().toString());
		
		resultClient = HttpClients.custom().setConnectionManager(config.getCm()).build();
		
		return resultClient;
	}
	
	public HttpClientContext getHttpContext(RoutingEntry routing, Environment targetEnv){
		
		HttpClientContext resultContext = null;
		
		// Add new empty configuration
		if (!httpClientConfigPool.containsKey(routing.getId().toString())){
			httpClientConfigPool.put(routing.getId().toString(), new HttpClientConfiguration());
		}
		HttpClientConfiguration config = httpClientConfigPool.get(routing.getId().toString());
		
		
		if (!config.getContextCache().containsKey(targetEnv.getKey())){
			if (targetEnv.getUsername() != null	&& targetEnv.getUsername().length() > 0) {

				// Create AuthCache instance
				//			AuthCache authCache = new BasicAuthCache();

				CredentialsProvider provider = new BasicCredentialsProvider();
				UsernamePasswordCredentials cred = new UsernamePasswordCredentials(
						targetEnv.getUsername(), targetEnv.getPw());
				provider.setCredentials(AuthScope.ANY, cred);

				// Add AuthCache to the execution context
				HttpClientContext context = HttpClientContext.create();
				context.setCredentialsProvider(provider);
				context.setAuthCache(config.getAuthCache());

				config.getContextCache().put(targetEnv.getKey(), context);

			} else {
				HttpClientContext context = HttpClientContext.create();
				config.getContextCache().put(targetEnv.getKey(), context);
			}
		}
		
		resultContext = config.getContextCache().get(targetEnv.getKey());
		
		return resultContext;
	}
	
	class HttpClientConfiguration {
		
		private PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		private AuthCache authCache = new BasicAuthCache();
		private Map<String, HttpClientContext> contextCache = new HashMap<String, HttpClientContext>();
		
		public PoolingHttpClientConnectionManager getCm() {
			return cm;
		}
		public void setCm(PoolingHttpClientConnectionManager cm) {
			this.cm = cm;
		}
		public AuthCache getAuthCache() {
			return authCache;
		}
		public void setAuthCache(AuthCache authCache) {
			this.authCache = authCache;
		}
		public Map<String, HttpClientContext> getContextCache() {
			return contextCache;
		}
		public void setContextCache(Map<String, HttpClientContext> contextCache) {
			this.contextCache = contextCache;
		}
		
	}
}
