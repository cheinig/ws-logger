package de.chdev.wslogger.worker;


import de.chdev.wslogger.model.Request;

import java.util.HashMap;
import java.util.Map;

public class ApplicationCache {
	
	private static ApplicationCache instance=null;
	
	private Map<String, Request> requestCacheMap;
	
	private ApplicationCache(){
		requestCacheMap = new HashMap<String, Request>();
	}
	
	public synchronized static ApplicationCache getInstance(){
		if (instance==null){
			instance=new ApplicationCache();
		}
		return instance;
	}
	
	public synchronized void addRequestCacheEntry(String groupKey, Request request){
		requestCacheMap.put(groupKey, request);
	}
	
	public synchronized Request getRequestCacheEntry(String groupKey){
		if (requestCacheMap.containsKey(groupKey)){
			return requestCacheMap.get(groupKey);
		} else {
			return null;
		}
	}
	
	public synchronized void removeRequestCacheEntry(String groupKey){
		if (requestCacheMap.containsKey(groupKey)){
			requestCacheMap.remove(groupKey);
		}
	}
}
