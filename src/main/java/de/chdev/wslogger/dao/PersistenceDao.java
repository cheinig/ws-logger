/**
 * 
 */
package de.chdev.wslogger.dao;


import de.chdev.wslogger.exception.PersistenceReadException;
import de.chdev.wslogger.exception.PersistenceWriteException;
import de.chdev.wslogger.model.*;

import java.util.List;

/**
 * @author Christoph Heinig
 *
 */
public interface PersistenceDao {
	
	Session getSession();
	
	// Request Operations
	Request getRequestById(Session session, Integer id) throws PersistenceReadException;
	List<Request> getLatestRequests(Session session, int counter) throws PersistenceReadException;
	Request saveRequest(Session session, Request request) throws PersistenceWriteException;
	List<Request> getRequestListByStatus(Session session, String status) throws PersistenceReadException;
	
	// Routing Operations
	List<RoutingEntry> getRoutingEntriesByClientIpAndEndpointAndOperation(Session session, String clientIp, String endpoint, String operation)  throws PersistenceReadException;
//	public List<RoutingEntry> getRoutingEntriesByGroupKey(Session session, String groupKey) throws PersistenceReadException;
	
	// AutoMapping Operations
	List<AutoMapping> getAutoMappingListByRoutingAndType(Session session, RoutingEntry routingEntry, String operation, String type) throws PersistenceReadException;
	
	// Environment Operations
	List<Environment> getEnvironmnetListByIP(Session session, String ip) throws PersistenceReadException;
	
	// MessageFilter Operations
	List<MessageFilter> getMessageFilterListByRouting(Session session, RoutingEntry routing) throws PersistenceReadException;
	
	// Sequence Operations
	Long getGeneratedTicketId(Session session) throws PersistenceReadException;

	Request mergeRequest(Session session, Request request) throws PersistenceWriteException;
}
