/**
 * 
 */
package de.chdev.wslogger.dao.hibernate;


import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.dao.Session;
import de.chdev.wslogger.exception.PersistenceConnectException;
import de.chdev.wslogger.exception.PersistenceValidationException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;

/**
 * @author Christoph Heinig
 * 
 */
public class SessionImpl implements Session {

	private org.hibernate.Session session = null;
	private Transaction transaction = null;
	private PersistenceDao persistenceDao = null;
	private SessionFactory sessionFactory;

	public SessionImpl(PersistenceDao persistenceDao, SessionFactory sessionFactory) {
		this.persistenceDao = persistenceDao;
		this.sessionFactory = sessionFactory;
	}

	public Session open() throws PersistenceConnectException {
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
		} catch (JDBCConnectionException jdbce) {
			throw new PersistenceConnectException("db.connectError", jdbce);
		}

		return this;
	}

	public void commit() throws PersistenceValidationException {
		try {
			if (transaction != null && transaction.getStatus().canRollback()) {
				transaction.commit();
			}
			if (session != null && session.isOpen()) {
				session.close();
			}
		} catch (ConstraintViolationException cve) {
			throw new PersistenceValidationException("validation error", cve, null);
		}
	}

	public void rollback() {
		if (transaction != null && transaction.getStatus().canRollback()) {
			transaction.rollback();
		}
		if (session != null && session.isOpen()) {
			session.close();
		}
	}

	public PersistenceDao getDao() {
		return persistenceDao;
	}

	public org.hibernate.Session getHibernateSession() {
		return session;
	}

	public void close() {
		if (session != null) {
			if (session.isOpen()) {
				// Search for open transactions
				if (session.getTransaction() != null && session.getTransaction().getStatus().canRollback()) {
					session.getTransaction().rollback();
				}
				session.close();
			}
		}
		session = null;
		transaction = null;
	}


}
