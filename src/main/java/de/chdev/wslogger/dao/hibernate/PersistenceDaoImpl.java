/**
 * 
 */
package de.chdev.wslogger.dao.hibernate;

import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.exception.PersistenceReadException;
import de.chdev.wslogger.exception.PersistenceWriteException;
import de.chdev.wslogger.model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christoph Heinig
 * 
 */
@Component
public class PersistenceDaoImpl implements PersistenceDao {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	/*
	 * ()
	 */
	@Override
	public de.chdev.wslogger.dao.Session getSession(){
		return new SessionImpl(this, entityManagerFactory.unwrap(SessionFactory.class));
	}


	@Override
	public Request getRequestById(de.chdev.wslogger.dao.Session session,
								  Integer id) throws PersistenceReadException {
		Request result = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			result = (Request)hibernateSession.get(Request.class, id);
		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return result;
	}

	@Override
	public List<Request> getLatestRequests(
			de.chdev.wslogger.dao.Session session, int counter)
			throws PersistenceReadException {
		
			List<Request> resultList = null;
			
			Session hibernateSession = ((SessionImpl) session).getHibernateSession();

			try {
				StringBuilder queryString = new StringBuilder();
				queryString.append("from Request where true=true");
					Query query = hibernateSession.createQuery(queryString.toString());
					resultList = (List<Request>)query.list();
			} catch (Exception e) {
				PersistenceReadException pce = new PersistenceReadException(
						"Error during read operation from the database subsystem", e);
				throw pce;
			}

			return resultList;
	}


	@Override
	public Request saveRequest(de.chdev.wslogger.dao.Session session,
			Request request) throws PersistenceWriteException {
		Request result = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			result = (Request) hibernateSession.merge(request);
		} catch (Exception e) {
			PersistenceWriteException pce = new PersistenceWriteException(
					"Error during write operation to the database subsystem", e);
			throw pce;
		}

		return result;
	}
	
	@Override
	public Request mergeRequest(de.chdev.wslogger.dao.Session session,
			Request request) throws PersistenceWriteException {
		Request result = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			result = (Request) hibernateSession.merge(request);
		} catch (Exception e) {
			PersistenceWriteException pce = new PersistenceWriteException(
					"Error during write operation to the database subsystem", e);
			throw pce;
		}

		return result;
	}


	@Override
	public List<RoutingEntry> getRoutingEntriesByClientIpAndEndpointAndOperation(
            de.chdev.wslogger.dao.Session session, String clientIp, String endpoint,
            String operation) throws PersistenceReadException {
		List<RoutingEntry> resultList = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("from RoutingEntry where ");
			if (endpoint!=null) {
				queryString.append("sourceEndpoint.endpointUrl=:endpoint");
			} else {
				queryString.append("sourceEndpoint=null");
			}
			if (operation!=null){
				queryString.append(" and operation=:operation");
			} 
			if (clientIp!=null) {
				queryString.append(" and sourceServer.ipv4=:clientIp");
			} 
			
				Query query = hibernateSession.createQuery(queryString.toString());
				if (endpoint!=null) query.setParameter("endpoint", endpoint);
				// Search for default target with endpointurl "null"
				if (endpoint==null && clientIp==null && operation==null) query.setParameter("endpoint", "null");
				if (operation!=null) query.setParameter("operation", operation);
				if (clientIp!=null) query.setParameter("clientIp", clientIp);
				List list = query.list();
				resultList = (List<RoutingEntry>)list;
		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return resultList;
	}


	@Override
	public List<AutoMapping> getAutoMappingListByRoutingAndType(
			de.chdev.wslogger.dao.Session session, RoutingEntry routingEntry, String operation,
			String type) throws PersistenceReadException {
		List<AutoMapping> resultList = new ArrayList<AutoMapping>();
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			StringBuilder queryString = new StringBuilder();
			Query query = null;
			queryString.append("from AutoMapping where type=:type");
			if (routingEntry!=null){
				queryString.append(" and routing.id=:routingId");
			} else {
				queryString.append(" and routing.id is null");
			}				
				
			if (operation!=null){
				queryString.append(" and operation=:operation");
			} else {
				queryString.append(" and operation is null");
			}	
				
				
				query = hibernateSession.createQuery(queryString.toString());
				if (routingEntry!=null) query.setParameter("routingId", (routingEntry!=null)?routingEntry.getId():null);
				query.setParameter("type", type);
				if (operation!=null) query.setParameter("operation", operation);
				resultList = (List<AutoMapping>)query.list();

		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return resultList;
	}


	@Override
	public List<Request> getRequestListByStatus(
			de.chdev.wslogger.dao.Session session, String status)
			throws PersistenceReadException {
		List<Request> resultList = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("from Request where status=:status");
				Query query = hibernateSession.createQuery(queryString.toString());
				query.setParameter("status", status);
				resultList = (List<Request>)query.list();
		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return resultList;
	}
	
	@Override
	public List<Environment> getEnvironmnetListByIP(
			de.chdev.wslogger.dao.Session session, String ip)
					throws PersistenceReadException {
		List<Environment> resultList = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("from Environment where ipv4=:ip or ipv6=:ip");
				Query query = hibernateSession.createQuery(queryString.toString());
				query.setParameter("ip", ip);
				resultList = (List<Environment>)query.list();
		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return resultList;
	}


	@Override
	public List<MessageFilter> getMessageFilterListByRouting(
			de.chdev.wslogger.dao.Session session, RoutingEntry routing)
			throws PersistenceReadException {
		List<MessageFilter> resultList = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("from MessageFilter where routing.id=:id");
				Query query = hibernateSession.createQuery(queryString.toString());
				query.setParameter("id", routing.getId());
				resultList = (List<MessageFilter>)query.list();
		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return resultList;
	}


	@Override
	public Long getGeneratedTicketId(de.chdev.wslogger.dao.Session session)
			throws PersistenceReadException {
		Long result = null;
		
		Session hibernateSession = ((SessionImpl) session).getHibernateSession();

		try {
			Query query =
			        hibernateSession.createSQLQuery("INSERT INTO autonum VALUES (0)")
			            .addScalar("num", StandardBasicTypes.LONG);
			query.executeUpdate();

			query = hibernateSession.createSQLQuery("SELECT LAST_INSERT_ID()");
			result =  ((Long) query.uniqueResult()).longValue();
		} catch (Exception e) {
			PersistenceReadException pce = new PersistenceReadException(
					"Error during read operation from the database subsystem", e);
			throw pce;
		}

		return result;
	}

}
