/**
 * 
 */
package de.chdev.wslogger.dao;


import de.chdev.wslogger.exception.PersistenceConnectException;
import de.chdev.wslogger.exception.WSLoggerBasicException;

/**
 * @author Christoph Heinig
 *
 */
public interface Session {
	
	Session open() throws PersistenceConnectException;
	
	PersistenceDao getDao();
	
	void commit() throws WSLoggerBasicException;
	
	void rollback();
	
	void close();

}
