package de.chdev.wslogger;

import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.servlet.MainServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WsLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsLoggerApplication.class, args);
	}


	@Bean
	public ServletRegistrationBean mainServletBean(PersistenceDao connection) {
		ServletRegistrationBean bean = new ServletRegistrationBean(
				new MainServlet(connection), "/*");
		bean.setLoadOnStartup(1);
		return bean;
	}
}
