/**
 * 
 */
package de.chdev.wslogger.exception;



/**
 * This exception class will be used for all situations the input validation fails.
 *
 * @author Christoph Heinig
 *
 */
public class InputValidationException extends WSLoggerBasicException {
	
	/**
     * The simple constructor of the exception will take only the error code key to read the related error data.
     *
	 * @param codeKey the error code key to get the related error data
	 */
	public InputValidationException(String codeKey) {
		super(codeKey, codeKey, null, new String[]{});
	}
	
	/**
     * This advanced constructor will take the error code key and additional parameters for the substitution of error messages.
     * It will also take the root cause of this exception.
	 * @param codeKey The error code key to get the related error data
	 * @param cause The root cause of this exception
	 * @param params Additional parameters which will be used for error message substitution
	 */
	public InputValidationException(String codeKey,
                                    Throwable cause, String[] params) {
		super(codeKey, codeKey, cause, params);
	}
	
	/**
     * This simplified constructor will take the error code key and 1 substitution parameter
	 * @param codeKey The error code key to get the related error data
     * @param param1 The substitution string
	 */
	public InputValidationException(String codeKey, String param1) {
		super(codeKey, codeKey, null, new String[]{param1});
	}
	
	/**
     * This simplified constructor will take the error code key and 2 substitution parameters
	 * @param codeKey The error code key to get the related error data
     * @param param1 The first substitution parameter
     * @param param2 The second substitution parameter
	 */
	public InputValidationException(String codeKey, String param1, String param2) {
		super(codeKey, codeKey, null, new String[]{param1, param2});
	}
	
	/**
     * This is the full exception constructor which will take an additional exception message and the error code key.
     * It will also take the root exception and an array of substitution parameters
	 * @param message An additional exception error message
	 * @param codeKey The error code key to get the related error data
	 * @param cause The root cause of this exception
	 * @param params An array of substitution parameters for the error message
	 */
	public InputValidationException(String message, String codeKey,
                                    Throwable cause, String[] params) {
		super(message, codeKey, cause, params);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
