/**
 * 
 */
package de.chdev.wslogger.exception;

/**
 * @author Christoph Heinig
 *
 */
public class PersistenceWriteException extends WSLoggerBasicException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    /**
     * This simple constructor will take an error message and a root cause
     * @param message The code defined error message
     * @param cause The root cause of this exception
     */
	public PersistenceWriteException(String message, Throwable cause){
		super(message, "db.writeError", cause, new String[]{});
	}

    /**
     * This full constructor will take an error message and the error code key to get related error data.
     * A root cause and also an array with error message substitution values can be provided.
     * @param message The code defined error message
     * @param codeKey The error code key to get the related error data
     * @param cause The root cause of this exception
     * @param params An array of substitution values for the error messages
     */
	public PersistenceWriteException(String message, String codeKey, Throwable cause, String[] params){
		super(message, codeKey, cause, params);
	}
	
}
