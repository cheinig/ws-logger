/**
 * 
 */
package de.chdev.wslogger.exception;

/**
 * This exception class will be used if the connection to the backend system failed
 *
 * @author Christoph Heinig
 *
 */
public class PersistenceConnectException extends WSLoggerBasicException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    /**
     * This simple constructor will take an error message and a root cause
     * @param message The code defined error message
     * @param cause The root cause of this exception
     */
	public PersistenceConnectException(String message, Throwable cause){
		super(message, "db.connectError", cause, new String[]{});
	}

    /**
     * This full constructor will take an error message and the error code key to get related error data.
     * A root cause and also an array with error message substitution values can be provided.
     * @param message The code defined error message
     * @param codeKey The error code key to get the related error data
     * @param cause The root cause of this exception
     * @param params An array of substitution values for the error messages
     */
	public PersistenceConnectException(String message, String codeKey, Throwable cause, String[] params){
		super(message, codeKey, cause, params);
	}
}
