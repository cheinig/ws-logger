/**
 * 
 */
package de.chdev.wslogger.exception;


/**
 * This exception class is the basic exception type of the nss user administration service.
 * All application exceptions are subclasses of this superclass.
 *
 * @author Christoph Heinig
 *
 */
public class WSLoggerBasicException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codeKey = null;
	
	protected Throwable cause = null;
	
	private String[] params = null;

    /**
     * Full basic exception constructor which takes all objects needed to handle application errors.
     * @param message The defined exception error message
     * @param codeKey The code key which will be used to read an error message from the error message repository
     * @param cause The exception causing this application exception
     * @param params Additional parameters to substitute the error messages from the message repository
     */
	public WSLoggerBasicException(String message, String codeKey, Throwable cause, String[] params){
		super(message, cause);
		this.cause = cause;
		this.codeKey=codeKey;
		this.setParams(params);
	}

    /**
     * Return the currently set error key code
     * @return the currently set error key code
     */
	public String getCodeKey() {
		return codeKey;
	}

//    /**
//     * Set a new error key code
//     * @param codeKey
//     */
//	public void setCodeKey(String codeKey) {
//		this.codeKey = codeKey;
//	}

    /**
     * Returns the currently set array of error message parameters
     * @return The currently set array of error message substitution parameters
     */
	public String[] getParams() {
		return params;
	}

    /**
     * Set a new string array of substitution parameters
     * @param params A string array of substitution parameters for the error messages
     */
	public void setParams(String[] params) {
		this.params = params;
	}
	
}
