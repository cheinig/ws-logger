/**
 * 
 */
package de.chdev.wslogger.exception;



/**
 * This exception class will be used for all service output validations
 *
 * @author Christoph Heinig
 *
 */
public class OutputValidationException extends WSLoggerBasicException {
	
	/**
     * This simple exception constructor will take the error code key to get the related error data
	 * @param codeKey The error code key to get the related error data
	 */
	public OutputValidationException(String codeKey) {
		super(codeKey, codeKey, null, new String[]{});
	}

    /**
     * This advanced constructor will take the error code key and additional parameters for the substitution of error messages.
     * It will also take the root cause of this exception.
     * @param codeKey The error code key to get the related error data
     * @param cause The root cause of this exception
     * @param params Additional parameters which will be used for error message substitution
     */
	public OutputValidationException(String codeKey,
                                     Throwable cause, String[] params) {
		super(codeKey, codeKey, cause, params);
	}

    /**
     * This simplified constructor will take the error code key and 1 substitution parameter
     * @param codeKey The error code key to get the related error data
     * @param param1 The substitution string
     */
	public OutputValidationException(String codeKey, String param1) {
		super(codeKey, codeKey, null, new String[]{param1});
	}

    /**
     * This simplified constructor will take the error code key and 2 substitution parameters
     * @param codeKey The error code key to get the related error data
     * @param param1 The first substitution parameter
     * @param param2 The second substitution parameter
     */
	public OutputValidationException(String codeKey, String param1, String param2) {
		super(codeKey, codeKey, null, new String[]{param1, param2});
	}

    /**
     * This is the full exception constructor which will take an additional exception message and the error code key.
     * It will also take the root exception and an array of substitution parameters
     * @param message An additional exception error message
     * @param codeKey The error code key to get the related error data
     * @param cause The root cause of this exception
     * @param params An array of substitution parameters for the error message
     */
	public OutputValidationException(String message, String codeKey,
                                     Throwable cause, String[] params) {
		super(message, codeKey, cause, params);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
