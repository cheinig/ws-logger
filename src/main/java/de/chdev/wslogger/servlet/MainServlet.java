package de.chdev.wslogger.servlet;

import de.chdev.wslogger.aswbxml.ActiveSyncWbxmlConverter;
import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.dao.Session;
import de.chdev.wslogger.exception.PersistenceConnectException;
import de.chdev.wslogger.exception.PersistenceReadException;
import de.chdev.wslogger.exception.PersistenceWriteException;
import de.chdev.wslogger.exception.WSLoggerBasicException;
import de.chdev.wslogger.model.*;
import de.chdev.wslogger.utils.Utils;
import de.chdev.wslogger.worker.ApplicationCache;
import de.chdev.wslogger.worker.SendWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainServlet extends HttpServlet {
	
	private static Log log = LogFactory.getLog(MainServlet.class);
	private PersistenceDao connection;
	private static final long serialVersionUID = 1L;

	public MainServlet(PersistenceDao connection) {
		this.connection = connection;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		log.info("doGet");
		String versionString = "unknown";
		try {
			versionString = Utils.getInstance().getVersionInfo();
		} catch (Exception e){
			versionString = e.getMessage();
		}
		resp.getOutputStream().print(versionString);
	}

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("doOptions");

		Session session=null;

		try {

			session = connection.getSession();

			Request request = new Request();
			request.setRequesttype("OPTIONS");
			StringBuilder tempStringBuilder;
			String requestContentType = "";

			//
			// Fill independent data
			//
			// Extract header data
			tempStringBuilder = new StringBuilder();
			Enumeration<String> headerNames = req.getHeaderNames();
			while (headerNames.hasMoreElements()){
				String nextElement = headerNames.nextElement();
				if (nextElement.equalsIgnoreCase("loggertarget")) request.setClient(req.getHeader(nextElement));
				if (nextElement.equalsIgnoreCase("content-type")) requestContentType = req.getHeader(nextElement);
				tempStringBuilder.append(nextElement+": "+req.getHeader(nextElement)+"\n");
			}
			request.setRequestheader(tempStringBuilder.toString());

			// Extract body data
//			if (!req.getHeader("content-type").equalsIgnoreCase("application/vnd.ms-sync.wbxml")) {
//				tempStringBuilder = new StringBuilder();
//				BufferedReader reader = req.getReader();
//				String line;
//				while ((line = reader.readLine()) != null) {
//					tempStringBuilder.append(line + "\n");
//				}
//				request.setRequest(tempStringBuilder.toString());
//			} else {
			// Extract raw body data
			ServletInputStream inputStream = req.getInputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			if (inputStream != null) {
				int singleByte = inputStream.read();
				while (singleByte != -1) {
					byteArrayOutputStream.write(singleByte);
					singleByte = inputStream.read();
				}
				request.setRequestraw(byteArrayOutputStream.toByteArray());
			}
			request.setRequest(Utils.getInstance().getDecodedString(request.getRequestraw(), requestContentType));
//			}

			// Set incoming date
			request.setCreatedate(new Date());
			// Set the called endpoint url
			request.setEndpoint(req.getRequestURI().substring(req.getContextPath().length()));
			// Set query params
			request.setRequestquery(req.getQueryString());
			// Set ip address of calling client if not overwritten by http header
			if (request.getClient()==null) request.setClient(req.getRemoteAddr());

			//
			// Fill data with dependency
			//
			// Calc Environment
			request.setSourceEnv(getSourceEnvironment(request));
			// Get Operation from soap request message
			request.setOperation(getOperationName(request.getRequest(), request.getRequestquery(), request.getRequesttype()));
			// Calc RoutingInfo
			request.setRouting(getRoutingEntry(request.getClient(), request.getEndpoint(), request.getOperation()));
			// Extract message filter values from REQUEST - needs routingid
			processMessageFilter(request, "REQUEST");
			// Set target environment - needs extracted groupid
			if (request.getRouting()!=null)	request.setTargetEnv(getTargetEnvironment(request));
			// Process AutoMapping
			proceedAutoResponse(request);
			// Calc Status
			request.setStatus(getCurrentStatus(request));

			// Save request to database
			session.open();
			request = connection.saveRequest(session, request);
			session.commit();

			// Process automatic callbacks before generating a response
			proceedAutoCallback(request);

			// Calc Response
			if (request.getStatus().equals("FWD")){
				SendWorker sender = new SendWorker(connection);
				request = sender.sendRequest(request);
				request.setStatus("FWD_DONE");
			} else {
				// Response will be an autoresponse, so replace placeholders
				request.setResponse(Utils.getInstance().prepareContent(request.getResponse(), request, connection));
			}
			setResponse(request, resp);

			// Set response header
			if (request.getStatus().equals("RECEIVED")){
				// Extract header data
				tempStringBuilder = new StringBuilder();
				Collection<String> respHeaderNames = resp.getHeaderNames();
				for (String respHeaderName : respHeaderNames){
					tempStringBuilder.append(respHeaderName+": "+resp.getHeader(respHeaderName)+"\n");
				}
				request.setResponseheader(tempStringBuilder.toString());
			}

			// Extract message filter values from RESPONSE
			processMessageFilter(request, "RESPONSE");

			session.open();
			request = connection.mergeRequest(session, request);
			session.commit();

//			// Process automatic callbacks
//			proceedAutoCallback(session, request);

			// Close DB session and commit

//			log.info("doPost for "+request.getOperation()+" ("+request.getGroupid()+") finished in: "+(System.currentTimeMillis()-timestamp));

		} catch (Exception e) {
			log.error("request can not be processed", e);
		} finally {
			if (session!=null)
				session.close();
		}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		log.info("doPut");
		super.doPut(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		log.info("doPost started");

		Session session=null;
		
		try {
			
			session = connection.getSession();

			Request request = new Request();
			request.setRequesttype("POST");
			StringBuilder tempStringBuilder = new StringBuilder();
			String contentType = "";

			//
			// Fill independent data
			//
			// Extract header data
			tempStringBuilder = new StringBuilder();
			Enumeration<String> headerNames = req.getHeaderNames();
			while (headerNames.hasMoreElements()){
				String nextElement = headerNames.nextElement();
				if (nextElement.equalsIgnoreCase("loggertarget")) request.setClient(req.getHeader(nextElement));
				if (nextElement.equalsIgnoreCase("content-type")) contentType = req.getHeader(nextElement);
				tempStringBuilder.append(nextElement+": "+req.getHeader(nextElement)+"\n");
			}
			request.setRequestheader(tempStringBuilder.toString());

			// Extract body data
//			if (!req.getHeader("content-type").equalsIgnoreCase("application/vnd.ms-sync.wbxml")) {
//				tempStringBuilder = new StringBuilder();
//				BufferedReader reader = req.getReader();
//				String line;
//				while ((line = reader.readLine()) != null) {
//					tempStringBuilder.append(line + "\n");
//				}
//				request.setRequest(tempStringBuilder.toString());
//			} else {
				// Extract raw body data
				ServletInputStream inputStream = req.getInputStream();
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				if (inputStream != null) {
					int singleByte = inputStream.read();
					while (singleByte != -1) {
						byteArrayOutputStream.write(singleByte);
						singleByte = inputStream.read();
					}
					request.setRequestraw(byteArrayOutputStream.toByteArray());
				}
				request.setRequest(Utils.getInstance().getDecodedString(request.getRequestraw(), contentType));
//			}

			// Set incoming date
			request.setCreatedate(new Date());
			// Set the called endpoint url
			request.setEndpoint(req.getRequestURI().substring(req.getContextPath().length()));
			// Set query params
			request.setRequestquery(req.getQueryString());
			// Set ip address of calling client if not overwritten by http header
			if (request.getClient()==null) request.setClient(req.getRemoteAddr());
			
			// 
			// Fill data with dependency
			//
			// Calc Environment
			request.setSourceEnv(getSourceEnvironment(request));
			// Get Operation from soap request message
			request.setOperation(getOperationName(request.getRequest(), request.getRequestquery(), request.getRequesttype()));
			// Calc RoutingInfo
			request.setRouting(getRoutingEntry(request.getClient(), request.getEndpoint(), request.getOperation()));
			// Extract message filter values from REQUEST - needs routingid
			processMessageFilter(request, "REQUEST");
			// Set target environment - needs extracted groupid
			if (request.getRouting()!=null)	request.setTargetEnv(getTargetEnvironment(request));
			// Process AutoMapping
			proceedAutoResponse(request);
			// Calc Status
			request.setStatus(getCurrentStatus(request));
			
			// Save request to database
			session.open();
			request = connection.saveRequest(session, request);
			session.commit();

			// Process automatic callbacks before generating a response
			proceedAutoCallback(request);

			// Calc Response
			if (request.getStatus().equals("FWD")){
				SendWorker sender = new SendWorker(connection);
				request = sender.sendRequest(request);
				request.setStatus("FWD_DONE");
			} else {
				// Response will be an autoresponse, so replace placeholders
				request.setResponse(Utils.getInstance().prepareContent(request.getResponse(), request, connection));
			}
			setResponse(request, resp);
			
			// Set response header
			if (request.getStatus().equals("RECEIVED")){
				// Extract header data
				tempStringBuilder = new StringBuilder();
				Collection<String> respHeaderNames = resp.getHeaderNames();
				for (String respHeaderName : respHeaderNames){
					tempStringBuilder.append(respHeaderName+": "+resp.getHeader(respHeaderName)+"\n");
				}
				request.setResponseheader(tempStringBuilder.toString());
			}

			// Extract message filter values from RESPONSE
			processMessageFilter(request, "RESPONSE");

			session.open();
			request = connection.mergeRequest(session, request);
			session.commit();

//			// Process automatic callbacks
//			proceedAutoCallback(session, request);
			
			// Close DB session and commit
			
//			log.info("doPost for "+request.getOperation()+" ("+request.getGroupid()+") finished in: "+(System.currentTimeMillis()-timestamp));
			
		} catch (Exception e) {
			log.error("request can not be processed", e);
		} finally {
			if (session!=null)
			session.close();
		}
		
	}

	private String getOperationName(String requestString, String requestQuery, String operation) {
		String operationName = "";

		if (requestString!=null) {
			Pattern pattern = null;
			pattern = Pattern.compile("<(\\w*:)?Envelope[^>]*>\\s*(<[^>]*>)?\\s*<(\\w*:)?Body[^>]*>\\s*<(\\w*:)?(\\w*)[^>]*>");
			Matcher matcher = null;
			matcher = pattern.matcher(requestString);
			if (matcher != null && matcher.find()) {
				operationName = matcher.group(5);
			}
		}

		if (operationName.isEmpty() && requestQuery != null){
			Pattern pattern = null;
			pattern = Pattern.compile("Cmd=(\\w*)");
			Matcher matcher = null;
			matcher = pattern.matcher(requestQuery);
			if (matcher != null && matcher.find()) {
				operationName = matcher.group(1);
			}
		}

		if (operationName.isEmpty() && operation.equalsIgnoreCase("options")){
			operationName = "OPTIONS";
		}

		return operationName;
	}

	public void processMessageFilter(Request request, String direction){
		
		Session session = null;

		try {
			session = connection.getSession().open();
			
			List<MessageFilter> messageFilterListByRouting = connection.getMessageFilterListByRouting(session, request.getRouting());
			
			session.commit();

			for (MessageFilter messageFilter : messageFilterListByRouting) {
				
				Pattern pattern = null;
				pattern = Pattern.compile(messageFilter.getRegex());
				
				Matcher matcher = null;
				if ((direction == null || direction.equals("REQUEST")) && messageFilter.getDirection().equals("REQUEST")){
					matcher = pattern.matcher(request.getRequest());
				} else if ((direction == null || direction.equals("RESPONSE")) && messageFilter.getDirection().equals("RESPONSE")){
					matcher = pattern.matcher(request.getResponse());
				}
				
				if (matcher != null && matcher.find()){
					
					switch (messageFilter.getIndexnumber()){
					case 0 : request.setGroupid(matcher.group(1)); break;
					case 1 : request.setValue01(matcher.group(1)); break;
					case 2 : request.setValue02(matcher.group(1)); break;
					case 3 : request.setValue03(matcher.group(1)); break;
					case 4 : request.setValue04(matcher.group(1)); break;
					case 5 : request.setValue05(matcher.group(1)); break;
					case 6 : request.setValue06(matcher.group(1)); break;
					case 7 : request.setValue07(matcher.group(1)); break;
					case 8 : request.setValue08(matcher.group(1)); break;
					case 9 : request.setValue09(matcher.group(1)); break;
					case 10 : request.setValue10(matcher.group(1)); break;
					default: 
					}
					
				}
			}
		} catch (PersistenceReadException | PersistenceConnectException e) {
			log.error("Value can not be extracted", e);
		} catch (WSLoggerBasicException e) {
			log.error("Database error during message filter processing", e);
		}
	}
	
	private Environment getTargetEnvironment(Request request) {
		Environment targetEnv = null;
		
		if (request.getRouting()!=null){
			if (request.getRouting().getTargetServer()!=null){
				targetEnv = request.getRouting().getTargetServer();
			} else if (request.getRouting().getLoopback()!=null && request.getRouting().getLoopback().equals("IN")){
				// Handle loopback routing
				Request requestCacheEntry = ApplicationCache.getInstance().getRequestCacheEntry(request.getRouting().getGroupkey()+""+request.getGroupid());
				if (requestCacheEntry!=null){
					targetEnv = requestCacheEntry.getSourceEnv();
				}
			}
		}
		
		
		return targetEnv;
	}

	private String getCurrentStatus(Request request) {
		String status = "RECEIVED";
		
		if (request.getTargetEnv()!=null){
			status="FWD";
		}
		
		return status;
	}
	
	private Environment getSourceEnvironment(Request request){
		Environment env = null;
		
		Session session = null;
		
		try {
			
			session = connection.getSession().open();
			
			List<Environment> environmnetList = connection.getEnvironmnetListByIP(session, request.getClient());
			if (environmnetList!=null && environmnetList.size()>0){
				env = environmnetList.get(0);
			}
			
			session.commit();
		} catch (PersistenceReadException | PersistenceConnectException e) {
			log.error("cannot get environment", e);
		} catch (WSLoggerBasicException e) {
			log.error("Database error during source environment process", e);
		}
		
		return env;
	}

	private String proceedAutoResponse(Request request){
		
		StringBuilder response=new StringBuilder();
		AutoMapping autoMapping = null;
		Session session = null;
		
		try {
			session = connection.getSession().open();
			// Try to fill automatic response
			List<AutoMapping> autoMappingList = connection.getAutoMappingListByRoutingAndType(session, request.getRouting()!=null?request.getRouting():null, request.getOperation(), "RESPONSE");
			if (autoMappingList == null || autoMappingList.size()<=0){
				autoMappingList = connection.getAutoMappingListByRoutingAndType(session, request.getRouting()!=null?request.getRouting():null, null, "RESPONSE");
			}
			if (autoMappingList == null || autoMappingList.size()<=0){
				autoMappingList = connection.getAutoMappingListByRoutingAndType(session, null, null, "RESPONSE");
			}
			
			session.commit();

			// Check if regex check is set
			for (AutoMapping autoMappingEntry : autoMappingList) {
				if (autoMappingEntry.getRegex()!=null && !autoMappingEntry.getRegex().isEmpty()){
					if (isRegexFitRequest(request.getRequest(), autoMappingEntry.getRegex())){
						autoMapping = autoMappingEntry;
						// leave the loop so that always the automapping entry with matching regex is used if available
						break;
					} else {
						// do nothing, no match
					}
				} else {
					autoMapping = autoMappingEntry;
				} 
			}
			
			if (autoMapping!=null){
				response.append(autoMapping.getTemplate().getTemplate());
				request.setResponseheader(autoMapping.getTemplate().getHeader());
				request.setResponseraw(autoMapping.getTemplate().getTemplateraw());
			}
			request.setResponse(response.toString());



			
		} catch (PersistenceReadException e) {
			log.error("Error during automapping",e);
		} catch (PersistenceConnectException e) {
			log.error("Error during automapping",e);
		} catch (WSLoggerBasicException e) {
			log.error("Database error during auto response process",e);
		} finally {
			if (session != null){
				session.close();
			}
		}
		
		return response.toString();
	}
	
	private boolean isRegexFitRequest(String request, String regex){
		boolean result=false;
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(request);
		result = matcher.find();
		
		return result;
	}
	
	
	private String proceedAutoCallback(Request request){
		
		StringBuilder response=new StringBuilder();
		AutoMapping autoMapping = null;
		Session session = null;
		
		try {
			
			session = connection.getSession().open();
			
			// Check if an async response should be sent
			List<AutoMapping> autoMappingList = connection.getAutoMappingListByRoutingAndType(session, request.getRouting()!=null?request.getRouting():null, request.getOperation(), "CALLBACK");
			if (autoMappingList == null || autoMappingList.size()<=0){
				autoMappingList = connection.getAutoMappingListByRoutingAndType(session, request.getRouting()!=null?request.getRouting():null, null, "CALLBACK");
			}
			if (autoMappingList == null || autoMappingList.size()<=0){
				autoMappingList = connection.getAutoMappingListByRoutingAndType(session, null, null, "CALLBACK");
			}
			
			session.commit();
			
			// Check if regex check is set
			for (AutoMapping autoMappingEntry : autoMappingList) {
				if (autoMappingEntry.getRegex()!=null && !autoMappingEntry.getRegex().isEmpty()){
					if (isRegexFitRequest(request.getRequest(), autoMappingEntry.getRegex())){
						autoMapping = autoMappingEntry;
						// leave the loop so that always the automapping entry with matching regex is used if available
						break;
					} else {
						// do nothing, no match
					}
				} else {
					autoMapping = autoMappingEntry;
				} 
			}
			
			if (autoMapping != null){
				String templateBody = autoMapping.getTemplate().getTemplate();
				byte[] templateBodyRaw = autoMapping.getTemplate().getTemplateraw();
				String templateHeader = autoMapping.getTemplate().getHeader();
//				Request relatedRequest = ApplicationCache.getInstance().getRequestCacheEntry(request.getRouting().getGroupkey());
				Request asyncRequest = new Request();
				asyncRequest.setClient("127.0.0.1");
				asyncRequest.setCreatedate(new Date());
				asyncRequest.setRouting(autoMapping.getTargetRouting());
				asyncRequest.setEndpoint(autoMapping.getTargetRouting().getTargetEndpoint().getEndpointUrl());
				asyncRequest.setRequest(templateBody);
				asyncRequest.setRequestraw(templateBodyRaw);
				asyncRequest.setRequestheader(templateHeader);
				
				ApplicationCache instance = ApplicationCache.getInstance();
				
				// The source environment must be set to the real target environment if it is a loopback routing
				if (ApplicationCache.getInstance().getRequestCacheEntry(request.getRouting().getGroupkey()+""+request.getGroupid())!=null){
					// Handle loopback routing
					Request requestCacheEntry = ApplicationCache.getInstance().getRequestCacheEntry(request.getRouting().getGroupkey()+""+request.getGroupid());
					asyncRequest.setSourceEnv(requestCacheEntry.getSourceEnv());
				} else {
					asyncRequest.setSourceEnv(request.getSourceEnv());
				}
				
				asyncRequest.setStatus("OUT");
				asyncRequest.setTargetEnv(autoMapping.getTargetRouting().getTargetServer());
				asyncRequest.setGroupid(request.getGroupid());
				asyncRequest.setValue01(request.getValue01());
				asyncRequest.setValue02(request.getValue02());
				asyncRequest.setValue03(request.getValue03());
				asyncRequest.setValue04(request.getValue04());
				asyncRequest.setValue05(request.getValue05());
				asyncRequest.setValue06(request.getValue06());
				asyncRequest.setValue07(request.getValue07());
				asyncRequest.setValue08(request.getValue08());
				asyncRequest.setValue09(request.getValue09());
				asyncRequest.setValue10(request.getValue10());
				asyncRequest.setOperation(getOperationName(asyncRequest.getRequest(), null, "POST"));
				
				session.open();
				
				asyncRequest = connection.saveRequest(session, asyncRequest);
				
				session.commit();
			}
			
		} catch (PersistenceReadException e) {
			log.error("Error during automapping",e);
		} catch (PersistenceConnectException e) {
			log.error("Error during automapping",e);
		} catch (PersistenceWriteException e) {
			log.error("Error during automapping",e);
		} catch (Exception e){
			log.error("Callback cannot be build", e);
		} finally {
			if (session!=null){
				session.close();
			}
		}
		
		return response.toString();
	}
	
	private String setResponse(Request request, HttpServletResponse resp){
		
		StringBuilder response=new StringBuilder();
		
		try {
//			if (request.getResponseraw()!=null){
				resp.setStatus(HttpStatus.SC_OK);
//				resp.setContentType("text/xml; charset=UTF-8");
			if (request.getResponseraw()!=null) {
				ServletOutputStream outputStream = resp.getOutputStream();
				outputStream.write(request.getResponseraw());
			}
			if (request.getResponseheader()!=null) {
				for (String header : request.getResponseheader().split("\n")){
					String key = header.substring(0, header.indexOf(':'));
					String value = header.substring(header.indexOf(':')+1);
					resp.setHeader(key.trim(), value.trim());
				}
			}
//			} else if (request.getResponse()!=null){
//				resp.setStatus(HttpStatus.SC_OK);
////				resp.setContentType("text/xml; charset=UTF-8");
//				PrintWriter writer = resp.getWriter();
//				writer.print(request.getResponse());
//				if (request.getResponseheader()!=null) {
//					for (String header : request.getResponseheader().split("\n")){
//						String key = header.substring(0, header.indexOf(':'));
//						String value = header.substring(header.indexOf(':')+1);
//						resp.setHeader(key.trim(), value.trim());
//					}
//				}
//				writer.close();
//			}
		} catch (IOException e) {
			log.error("Cannot write response", e);
		} 
		
		return response.toString();

	}

	private RoutingEntry getRoutingEntry(String clientIp, String endpointUrl, String operation) throws PersistenceReadException, PersistenceConnectException{
		RoutingEntry resultRoutingEntry = null;
		Session session = null;

		// Get all routing entries for this endpointUrl

		try {
			log.debug("load general routing info");
			session = connection.getSession().open();
			List<RoutingEntry> generalRoutingEntries = connection.getRoutingEntriesByClientIpAndEndpointAndOperation(session, null, endpointUrl, null);
			session.commit();
			for (RoutingEntry routingEntry : generalRoutingEntries){
				log.debug("check routing "+routingEntry.getId());
				log.debug("no proxy routing");
				// Check for client ip match
				if (routingEntry.getSourceServer()!=null && ((routingEntry.getSourceServer().getIpv4()!=null && 
						routingEntry.getSourceServer().getIpv4().equals(clientIp)) || (routingEntry.getSourceServer().getIpv6()!=null && 
						routingEntry.getSourceServer().getIpv6().equals(clientIp)))){
					log.debug("client match routing");
					// replace only if the currently existing one does not match
					if (resultRoutingEntry==null || resultRoutingEntry.getSourceServer()==null){
						log.debug("Set routing "+routingEntry.getId());
						resultRoutingEntry = routingEntry;
					}
				}
				// Check for operation match
				if (routingEntry.getOperation()!=null && operation != null && operation.matches(routingEntry.getOperation())){
					log.debug("operation match routing");
					// replace only if the currently existing one does not have an ip match
					if (resultRoutingEntry==null || resultRoutingEntry.getSourceEndpoint()==null || (routingEntry.getSourceServer()!=null && ((routingEntry.getSourceServer().getIpv4()!=null && 
							routingEntry.getSourceServer().getIpv4().equals(clientIp)) || (routingEntry.getSourceServer().getIpv6()!=null && 
							routingEntry.getSourceServer().getIpv6().equals(clientIp))))){
						log.debug("Set routing "+routingEntry.getId());
						resultRoutingEntry = routingEntry;
					}
				}
				//			}
				// Check if routing is a general routing
				if (resultRoutingEntry==null && routingEntry.getSourceServer()==null && routingEntry.getOperation()==null){
					log.debug("Set routing "+routingEntry.getId());
					resultRoutingEntry = routingEntry;
				}
			}
		} catch (WSLoggerBasicException e) {
			log.error("Database error during routing search", e);
		} finally {
			if (session != null){
				session.close();
			}
		}

		return resultRoutingEntry;
	}
	

	
}
