package de.chdev.wslogger.jobs;

import de.chdev.wslogger.dao.PersistenceDao;
import de.chdev.wslogger.worker.SendWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SendJob {
	
	Log log = LogFactory.getLog(SendJob.class);

	@Autowired
	PersistenceDao connection;

	@Scheduled(fixedDelay = 60000)
	public void execute() {
		log.info("Executing Job");
		SendWorker sendWorker = new SendWorker(connection);
		sendWorker.sendAllOutRequests();
	}


}
