package de.chdev.wslogger.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "template")
public class Template {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
    Long id;
	
	@Lob
	private String template;
	@Lob
	private byte[] templateraw;
	private String header;

	String label01;
	String label02;
	String label03;
	String label04;
	String label05;
	String label06;
	String label07;
	String label08;
	String label09;
	String label10;
	
	String value01;
	String value02;
	String value03;
	String value04;
	String value05;
	String value06;
	String value07;
	String value08;
	String value09;
	String value10;

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel01() {
		return label01;
	}

	public void setLabel01(String label01) {
		this.label01 = label01;
	}

	public String getLabel02() {
		return label02;
	}

	public void setLabel02(String label02) {
		this.label02 = label02;
	}

	public String getLabel03() {
		return label03;
	}

	public void setLabel03(String label03) {
		this.label03 = label03;
	}

	public String getLabel04() {
		return label04;
	}

	public void setLabel04(String label04) {
		this.label04 = label04;
	}

	public String getLabel05() {
		return label05;
	}

	public void setLabel05(String label05) {
		this.label05 = label05;
	}

	public String getLabel06() {
		return label06;
	}

	public void setLabel06(String label06) {
		this.label06 = label06;
	}

	public String getLabel07() {
		return label07;
	}

	public void setLabel07(String label07) {
		this.label07 = label07;
	}

	public String getLabel08() {
		return label08;
	}

	public void setLabel08(String label08) {
		this.label08 = label08;
	}

	public String getLabel09() {
		return label09;
	}

	public void setLabel09(String label09) {
		this.label09 = label09;
	}

	public String getLabel10() {
		return label10;
	}

	public void setLabel10(String label10) {
		this.label10 = label10;
	}

	public String getValue01() {
		return value01;
	}

	public void setValue01(String value01) {
		this.value01 = value01;
	}

	public String getValue02() {
		return value02;
	}

	public void setValue02(String value02) {
		this.value02 = value02;
	}

	public String getValue03() {
		return value03;
	}

	public void setValue03(String value03) {
		this.value03 = value03;
	}

	public String getValue04() {
		return value04;
	}

	public void setValue04(String value04) {
		this.value04 = value04;
	}

	public String getValue05() {
		return value05;
	}

	public void setValue05(String value05) {
		this.value05 = value05;
	}

	public String getValue06() {
		return value06;
	}

	public void setValue06(String value06) {
		this.value06 = value06;
	}

	public String getValue07() {
		return value07;
	}

	public void setValue07(String value07) {
		this.value07 = value07;
	}

	public String getValue08() {
		return value08;
	}

	public void setValue08(String value08) {
		this.value08 = value08;
	}

	public String getValue09() {
		return value09;
	}

	public void setValue09(String value09) {
		this.value09 = value09;
	}

	public String getValue10() {
		return value10;
	}

	public void setValue10(String value10) {
		this.value10 = value10;
	}


	public byte[] getTemplateraw() {
		return templateraw;
	}

	public void setTemplateraw(byte[] templateraw) {
		this.templateraw = templateraw;
	}
}
