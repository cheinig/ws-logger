package de.chdev.wslogger.model;

import javax.persistence.*;
import java.util.Date;

@javax.persistence.Entity
@Table(name = "request")
public class Request {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
    Long id;
	
	@Lob
    String request;
	@Lob
	byte[] requestraw;
	@Lob
    String response;
	@Lob
	byte[] responseraw;
	Date createdate;
	String requestheader;
	private String responseheader;
	String requesttype;
	String requestquery;
	String operation;
	String groupid;
	String endpoint;
	private String client;
	private String status;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sourceenv")
	private Environment sourceEnv;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "targetenv")
	private Environment targetEnv;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "routingid")
	private RoutingEntry routing;
	
	String value01;
	String value02;
	String value03;
	String value04;
	String value05;
	String value06;
	String value07;
	String value08;
	String value09;
	String value10;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getRequestheader() {
		return requestheader;
	}
	public void setRequestheader(String requestheader) {
		this.requestheader = requestheader;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public RoutingEntry getRouting() {
		return routing;
	}
	public void setRouting(RoutingEntry routing) {
		this.routing = routing;
	}
	public String getResponseheader() {
		return responseheader;
	}
	public void setResponseheader(String responseheader) {
		this.responseheader = responseheader;
	}
	public Environment getSourceEnv() {
		return sourceEnv;
	}
	public void setSourceEnv(Environment sourceEnv) {
		this.sourceEnv = sourceEnv;
	}
	public Environment getTargetEnv() {
		return targetEnv;
	}
	public void setTargetEnv(Environment targetEnv) {
		this.targetEnv = targetEnv;
	}
	public String getValue01() {
		return value01;
	}
	public void setValue01(String value01) {
		this.value01 = value01;
	}
	public String getValue02() {
		return value02;
	}
	public void setValue02(String value02) {
		this.value02 = value02;
	}
	public String getValue03() {
		return value03;
	}
	public void setValue03(String value03) {
		this.value03 = value03;
	}
	public String getValue04() {
		return value04;
	}
	public void setValue04(String value04) {
		this.value04 = value04;
	}
	public String getValue05() {
		return value05;
	}
	public void setValue05(String value05) {
		this.value05 = value05;
	}
	public String getValue06() {
		return value06;
	}
	public void setValue06(String value06) {
		this.value06 = value06;
	}
	public String getValue07() {
		return value07;
	}
	public void setValue07(String value07) {
		this.value07 = value07;
	}
	public String getValue08() {
		return value08;
	}
	public void setValue08(String value08) {
		this.value08 = value08;
	}
	public String getValue09() {
		return value09;
	}
	public void setValue09(String value09) {
		this.value09 = value09;
	}
	public String getValue10() {
		return value10;
	}
	public void setValue10(String value10) {
		this.value10 = value10;
	}
	public String getRequesttype() {
		return requesttype;
	}

	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}
	public byte[] getRequestraw() {
		return requestraw;
	}

	public void setRequestraw(byte[] requestraw) {
		this.requestraw = requestraw;
	}
	public byte[] getResponseraw() {
		return responseraw;
	}

	public void setResponseraw(byte[] responseraw) {
		this.responseraw = responseraw;
	}
	public String getRequestquery() {
		return requestquery;
	}

	public void setRequestquery(String requestquery) {
		this.requestquery = requestquery;
	}
}



