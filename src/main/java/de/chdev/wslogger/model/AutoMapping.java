package de.chdev.wslogger.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "automapping")
public class AutoMapping {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
    Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "routing_id")
	private
	RoutingEntry routing;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "templateid")
	Template template;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "target_routing_id")
	private	
	RoutingEntry targetRouting;
	
	private String operation;
	
	private String regex;
	
	
	private String type;
	
	public Template getTemplate() {
		return template;
	}
	
	public void setTemplate(Template template) {
		this.template = template;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public RoutingEntry getRouting() {
		return routing;
	}

	public void setRouting(RoutingEntry routing) {
		this.routing = routing;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public RoutingEntry getTargetRouting() {
		return targetRouting;
	}

	public void setTargetRouting(RoutingEntry targetRouting) {
		this.targetRouting = targetRouting;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}
}
