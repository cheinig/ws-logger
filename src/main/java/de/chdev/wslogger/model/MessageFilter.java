package de.chdev.wslogger.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "messagefilter")
public class MessageFilter {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

	String label;
	String regex;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "routingid")
	RoutingEntry routing;
	String direction;
	Integer indexnumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public RoutingEntry getRouting() {
		return routing;
	}

	public void setRouting(RoutingEntry routing) {
		this.routing = routing;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public Integer getIndexnumber() {
		return indexnumber;
	}

	public void setIndexnumber(Integer indexnumber) {
		this.indexnumber = indexnumber;
	}
}
