package de.chdev.wslogger.model;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "routinginfo")
public class RoutingEntry {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
    Long id;
	
	String label;
	String operation;
	String loopback;
	String groupkey;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "targetendpoint")
	private	Endpoint targetEndpoint;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sourceendpoint")
	private Endpoint sourceEndpoint;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sourceenv")
	private Environment sourceServer;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "targetenv")
	private Environment targetServer;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public Endpoint getTargetEndpoint() {
		return targetEndpoint;
	}
	public void setTargetEndpoint(Endpoint targetEndpoint) {
		this.targetEndpoint = targetEndpoint;
	}
	public Endpoint getSourceEndpoint() {
		return sourceEndpoint;
	}
	public void setSourceEndpoint(Endpoint sourceEndpoint) {
		this.sourceEndpoint = sourceEndpoint;
	}
	public Environment getSourceServer() {
		return sourceServer;
	}
	public void setSourceServer(Environment sourceServer) {
		this.sourceServer = sourceServer;
	}
	public Environment getTargetServer() {
		return targetServer;
	}
	public void setTargetServer(Environment targetServer) {
		this.targetServer = targetServer;
	}
	public String getLoopback() {
		return loopback;
	}
	public void setLoopback(String loopback) {
		this.loopback = loopback;
	}
	public String getGroupkey() {
		return groupkey;
	}
	public void setGroupkey(String groupkey) {
		this.groupkey = groupkey;
	}

	

}
